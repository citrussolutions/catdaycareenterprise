﻿using System.Linq;
using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace CatDayCareEnterprise
{
    public class Event
    {
        [Key]
        [Required]
        public long ID { get; set; }

        [Required]
        public long CatID { get; set; }

        [Required]
        public string Type { get; set; }

        [Required]
        public DateTime Timestamp { get; set; }
    }

}

