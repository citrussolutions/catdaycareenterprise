﻿using Microsoft.Data.Sqlite;
using System;
using Dapper;
using System.Data;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.Extensions.Configuration;

namespace CatDayCareEnterprise.Controllers
{
    public class DayCareController : Controller
    {
        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }


        [HttpGet]
        public IActionResult Register()
        {
            return View(new Cat());
        }


        [HttpPost]
        public async Task<IActionResult> Register(Cat model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            using (IDbConnection con = new SqliteConnection(Startup.Configuration.GetConnectionString("CatDayCareEnterprise")))
            {
                // validate e-mail address
                if (!string.IsNullOrEmpty(model.OwnerEmail))
                {
                    if (!new EmailAddressAttribute().IsValid(model.OwnerEmail))
                    {
                        ModelState.AddModelError("OwnerEmail", "Please check the e-mail format!");
                        return View(model);
                    }
                }

                // validate RFID
                IEnumerable<Cat> cats = await con.QueryAsync<Cat>($"SELECT * FROM Cat WHERE RFID = '{model.RFID}'");
                if (cats.Any())
                {
                    ModelState.AddModelError("RFID", "RFID already exists!");
                    return View(model);
                }

                await con.ExecuteAsync($"INSERT INTO Cat (Name, RFID, OwnerName, OwnerEmail) VALUES ('{model.Name}', '{model.RFID}', '{model.OwnerName}', '{model.OwnerEmail}')");
            }

            return RedirectToAction("Index");
        }

        [HttpGet]
        public async Task<IActionResult> CatList()
        {
            using (IDbConnection con = new SqliteConnection(Startup.Configuration.GetConnectionString("CatDayCareEnterprise")))
            {
                IEnumerable<Cat> cats = await con.QueryAsync<Cat>("SELECT * FROM Cat");

                return View(cats);
            }
        }

        [HttpGet]
        public IActionResult CheckInCheckOut()
        {
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> Search(string rfid)
        {
            using (IDbConnection con = new SqliteConnection(Startup.Configuration.GetConnectionString("CatDayCareEnterprise")))
            {
                Cat cat = (await con.QueryAsync<Cat>($"SELECT * FROM Cat WHERE RFID = '{rfid}'")).FirstOrDefault();

                if (cat != null)
                {
                    return Json(
                        new
                        {
                            id = cat.ID,
                            name = cat.Name,
                            rfid = cat.RFID
                        });
                }
                else
                {
                    return Json(
                        new
                        {
                            id = (long?)null,
                            name = (string)null,
                            rfid = (string)null
                        });
                }
            }
        }

        [HttpPost]
        public async Task<IActionResult> CheckIn(long id)
        {
            await RegisterActionAsync(id, "CHECK-IN");
            return Json("OK");
        }

        [HttpPost]
        public async Task<IActionResult> CheckOut(long id)
        {
            await RegisterActionAsync(id, "CHECK-OUT");
            return Json("OK");
        }

        private static async Task RegisterActionAsync(long id, string type)
        {
            using (IDbConnection con = new SqliteConnection(Startup.Configuration.GetConnectionString("CatDayCareEnterprise")))
            {
                await con.ExecuteAsync($"INSERT INTO Event (CatID, Type, Timestamp) VALUES ({id}, '{type}', '{DateTime.Now:s}')");
            }
        }

    }

}