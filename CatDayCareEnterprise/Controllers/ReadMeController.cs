﻿using Microsoft.AspNetCore.Mvc;

namespace CatDayCareEnterprise.Controllers
{
    public class ReadMeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}
