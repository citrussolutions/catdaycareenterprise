﻿///<reference path="typings\globals/angular/index.d.ts" />
///<reference path="typings\globals/jquery/index.d.ts" />

namespace CatDayCareEnterprise {

    const module: ng.IModule = angular.module("CatDayCareEnterprise", []);

    interface ICat {
        id: number;
        name: string;
        rfid: string;
    }

    /**
     * Check-in / Check-out controller
     */
    class CheckInCheckOutController {
        data: ICat = { id: null, name: null, rfid: null };

        static $inject = ["api"];

        constructor(private api: Api) {
        }

        searchCat() {
            this.api.searchCat(this.data.rfid, (result: ICat) => {
                this.data = result;
            });
        }

        checkIn() {
            this.api.checkIn(this.data);
        }

        checkOut() {
            this.api.checkOut(this.data);
        }
    }

    module.controller("checkInCheckOutController", CheckInCheckOutController);

    /**
     * Our backend API
     */
    class Api {
        static $inject = ["$http"];

        constructor(private $http: ng.IHttpService) {
        }

        searchCat(rfid: string, callback: (result: ICat) => void) {
            this.$http.get<any>("/DayCare/Search?rfid=" + encodeURIComponent(rfid), { headers: { 'Content-Type': "application/json" } })
                .success((result: any) => {
                    callback(result);
                }).error((data, status) => {
                    console.log(data);
                });
        }

        checkIn(cat: ICat) {
            this.$http.post<any>("/DayCare/CheckIn?id=" + cat.id, { headers: { 'Content-Type': "application/json" } })
                .success((result: any) => {
                }).error((data, status) => {
                    console.log(data);
                });
        }

        checkOut(cat: ICat) {
            this.$http.post<any>("/DayCare/CheckOut?id=" + cat.id, { headers: { 'Content-Type': "application/json" } })
                .success((result: any) => {
                }).error((data, status) => {
                    console.log(data);
                });
        }

    }

    module.service("api", Api);
}