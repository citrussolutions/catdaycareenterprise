﻿using System.ComponentModel.DataAnnotations;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CatDayCareEnterprise
{

    public class Cat
    {
        [Key]
        [Required]
        public long ID { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string RFID { get; set; }

        [Required]
        public string OwnerName { get; set; }

        public string OwnerEmail { get; set; }
    }
}
