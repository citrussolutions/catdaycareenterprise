var CatDayCareEnterprise;
(function (CatDayCareEnterprise) {
    var module = angular.module("CatDayCareEnterprise", []);
    var CheckInCheckOutController = (function () {
        function CheckInCheckOutController(api) {
            this.api = api;
            this.data = { id: null, name: null, rfid: null };
        }
        CheckInCheckOutController.prototype.searchCat = function () {
            var _this = this;
            this.api.searchCat(this.data.rfid, function (result) {
                _this.data = result;
            });
        };
        CheckInCheckOutController.prototype.checkIn = function () {
            this.api.checkIn(this.data);
        };
        CheckInCheckOutController.prototype.checkOut = function () {
            this.api.checkOut(this.data);
        };
        return CheckInCheckOutController;
    }());
    CheckInCheckOutController.$inject = ["api"];
    module.controller("checkInCheckOutController", CheckInCheckOutController);
    var Api = (function () {
        function Api($http) {
            this.$http = $http;
        }
        Api.prototype.searchCat = function (rfid, callback) {
            this.$http.get("/DayCare/Search?rfid=" + encodeURIComponent(rfid), { headers: { 'Content-Type': "application/json" } })
                .success(function (result) {
                callback(result);
            }).error(function (data, status) {
                console.log(data);
            });
        };
        Api.prototype.checkIn = function (cat) {
            this.$http.post("/DayCare/CheckIn?id=" + cat.id, { headers: { 'Content-Type': "application/json" } })
                .success(function (result) {
            }).error(function (data, status) {
                console.log(data);
            });
        };
        Api.prototype.checkOut = function (cat) {
            this.$http.post("/DayCare/CheckOut?id=" + cat.id, { headers: { 'Content-Type': "application/json" } })
                .success(function (result) {
            }).error(function (data, status) {
                console.log(data);
            });
        };
        return Api;
    }());
    Api.$inject = ["$http"];
    module.service("api", Api);
})(CatDayCareEnterprise || (CatDayCareEnterprise = {}));
